﻿using System.Collections.Generic;

namespace TheBackupApp.ClientAgent
{
    public class BackupConfigDTO
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public string ScheduledAt { get; set; }
        public int BackupType { get; set; }  // 0=Full, 1=Diff, 2=Incr
        public string DestinationPath { get; set; }
        public int SourceAccessUserId { get; set; }
        public int DestinationAccessUserId { get; set; }
        public bool Enabled { get; set; }
        public IEnumerable<string> SourcePaths { get; set; }
    }
}
