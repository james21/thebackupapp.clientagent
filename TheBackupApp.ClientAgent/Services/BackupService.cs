﻿using System;
using System.Diagnostics;
using System.IO;

namespace TheBackupApp.ClientAgent.Services
{
    internal static class BackupService
    {
        public static void RunBackup(string source, string destination)
        {
            string FolderPath = Environment.MachineName;

            if (!(Directory.Exists(destination + @"\" + FolderPath)))
                Directory.CreateDirectory(destination + @"\" + FolderPath);

            //Call a method to perform Xcopy
            ProcessXcopy(source, destination + @"\" + FolderPath);

            Console.WriteLine("We are done with the XCopy.");
        }

        /// <summary>
        /// Method to Perform Xcopy to copy files/folders from Source machine to Target Machine.
        /// Credit: http://www.c-sharpcorner.com/uploadfile/jawedmd/xcopy-using-c-sharp-to-copy-filesfolders/
        /// </summary>
        /// <param name="sourceDirectory"></param>
        /// <param name="targetDirectory"></param>
        private static void ProcessXcopy(string sourceDirectory, string targetDirectory)
        {
            // Use ProcessStartInfo class
            var startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;

            //Give the name as Xcopy
            startInfo.FileName = "xcopy";

            // TODO: Assign user impersonation here.

            //make the window Hidden
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;

            //Send the Source and destination as Arguments to the process
            startInfo.Arguments = "\"" + sourceDirectory + "\"" + " " + "\"" + targetDirectory + "\"" + @" /e /y /I";
            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    Debug.Assert(exeProcess != null, "exeProcess != null");
                    exeProcess.WaitForExit();
                }
            }
            catch (Exception exp)
            {
                Trace.Write(exp.Message);
                throw;
            }
        }
    }
}
