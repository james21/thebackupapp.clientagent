﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Runtime.Remoting;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TheBackupApp.ClientAgent.Services
{
    internal class CommunicationService
    {
        private readonly string _localIp;
        private readonly HttpClient _httpClient;

        public CommunicationService(string localIp, string baseAddress = "http://localhost:3473/api/")
        {
            _localIp = localIp;
            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(baseAddress)
            };
        }

        public async Task<bool> IsClientRegisteredAsync()
        {
            HttpResponseMessage isRegisteredResponse = await _httpClient.GetAsync($"Client?ip={_localIp}");

            if (isRegisteredResponse.IsSuccessStatusCode)
            {
                //return JsonConvert.DeserializeObject<bool>(response.Content.ReadAsStringAsync().Result);
                return bool.Parse(await isRegisteredResponse.Content.ReadAsStringAsync());
            }

            throw new ServerException(isRegisteredResponse.ReasonPhrase);
        }

        public async Task<IEnumerable<BackupConfigDTO>> FetchBackupConfigurationAsync()
        {
            HttpResponseMessage configResponse = _httpClient.GetAsync($"BackupConfiguration/GetByIp?ip={_localIp}").Result;

            if (configResponse.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<IEnumerable<BackupConfigDTO>>(await configResponse.Content.ReadAsStringAsync());
            }

            throw new ServerException(configResponse.ReasonPhrase);
        }

        public async Task<bool> RegisterClientAsync()
        {
            HttpResponseMessage registeredResponse = await _httpClient.PostAsJsonAsync("Client", new
            {
                Ip = _localIp,
                SystemName = Environment.MachineName
            });

            if (registeredResponse.IsSuccessStatusCode)
            {
                return true;
            }

            throw new ServerException(registeredResponse.ReasonPhrase);
        }

        public async Task<bool> LogActivityAsync(string message)
        {
            HttpResponseMessage registeredResponse = await _httpClient.PostAsJsonAsync("Client/Activities", new
            {
                ClientIp = _localIp,
                Message = $"[{Environment.MachineName} says]: {message}"
            });

            if (registeredResponse.IsSuccessStatusCode)
            {
                return true;
            }

            throw new ServerException(registeredResponse.ReasonPhrase);
        }
    }
}
