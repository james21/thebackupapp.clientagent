﻿using System;
using System.Globalization;
using Microsoft.Win32.TaskScheduler;
using Task = Microsoft.Win32.TaskScheduler.Task;

namespace TheBackupApp.ClientAgent.Services
{
    /// <summary>
    /// Uses Managed Task Scheduler Wrapper:
    /// https://www.nuget.org/packages/TaskScheduler/
    /// </summary>
    public static class SchedulerService
    {
        private const string TaskName = "TheBackupAppTask";

        #region Public
        public static void ConfigureScheduler(string command, double scheduledTime, int repeatIntervalInHours)
        {
            // Get the service on the local machine
            using (var ts = new TaskService())
            {
                ConfigureTask(command, scheduledTime, repeatIntervalInHours, ts);
            }
        }

        public static string GetCurrentScheduledTime()
        {
            // Get the service on the local machine
            using (var ts = new TaskService())
            {
                TimeTrigger tt = GetExistingTimeTrigger(ts);

                if (tt == null)
                    return "0";

                // Handle minute to military hour format
                int minutes = tt.StartBoundary.Minute;
                string minuteHrs = minutes < 10 ? "0" + minutes : minutes.ToString();
                return $"{tt.StartBoundary.Hour}.{minuteHrs}";
            }
        }

        private static void ConfigureTask(string command, double scheduledTime, int repeatIntervalInHours, TaskService ts)
        {
            // Check if task exists or Create a new task definition and assign properties
            Task existingTask = ts.GetTask(TaskName);

            TaskDefinition td = existingTask?.Definition ?? ts.NewTask();
            td.RegistrationInfo.Description = TaskName;

            // Remove existing triggers
            RemoveExistingTimeTriggers(existingTask);
            TimeTrigger tt = GetConfiguredTimeTrigger(scheduledTime, repeatIntervalInHours);
            td.Triggers.Add(tt);

            // Don't create redundant actions
            if (existingTask == null)
            {
                // Create an action that will launch Notepad whenever the trigger fires
                td.Actions.Add(new ExecAction(command, $"c:\\{TaskName}.log"));
            }

            // Register the task in the root folder
            ts.RootFolder.RegisterTaskDefinition(TaskName, td);

            existingTask?.RegisterChanges();
        }

        #endregion

        #region Private
        /// <summary>
        /// See detailed configuration at: 
        /// http://taskscheduler.codeplex.com/wikipage?title=TriggerSamples&referringTitle=Examples
        /// </summary>
        private static TimeTrigger GetConfiguredTimeTrigger(double scheduledTime = 0.0, int repeatIntervalInHours = 1)
        {
            var tt = new TimeTrigger();

            // **** V1 and V2 properties ******************************************
            // Disable the trigger from firing the task
            //tt.Enabled = false; // Default is true
            string[] timeSplits = scheduledTime.ToString(CultureInfo.InvariantCulture).Split('.');
            int hours = int.Parse(timeSplits[0]);
            int minutes = timeSplits.Length == 1 ? 0 : int.Parse(timeSplits[1]);

            tt.StartBoundary = DateTime.Today + TimeSpan.FromHours(hours) + TimeSpan.FromMinutes(minutes); // Default is the time the trigger is instantiated

            // Set the last time the trigger will run to a year from now.
            tt.EndBoundary = DateTime.Today + TimeSpan.FromDays(365); // Default is DateTime.MaxValue (or forever)

            // Set the time in between each repetition of the task after it starts to 30 minutes.
            tt.Repetition.Interval = TimeSpan.FromHours(repeatIntervalInHours); // Default is TimeSpan.Zero (or never)

            // **** V2 only properties ********************************************
            // Set an identifier to be used when logging
            tt.Id = TaskName;

            // Set the maximum time this task can run once triggered to one hour.
            tt.ExecutionTimeLimit = TimeSpan.FromHours(1); // Default is TimeSpan.Zero (or never)

            return tt;
        }

        private static void RemoveExistingTimeTriggers(Task myTask)
        {
            if (myTask == null) return;

            // Check to ensure we have a trigger and it is the one want
            if (myTask.Definition.Triggers.Count > 0 && myTask.Definition.Triggers[0] is TimeTrigger)
            {
                // Remove current task
                myTask.Definition.Triggers.RemoveAt(0);
            }

            // Register the changes (Note: if there is a password associated with the task, 
            // we will need to register using the TaskFolder.RegisterTaskDefinition method.
            myTask.RegisterChanges();
        }

        private static TimeTrigger GetExistingTimeTrigger(TaskService ts)
        {
            Task existingTask = ts.GetTask(TaskName);

            // Check to ensure we have a trigger and it is the one want
            if (existingTask?.Definition.Triggers.Count > 0 && existingTask.Definition.Triggers[0] is TimeTrigger)
            {
                return (TimeTrigger)existingTask.Definition.Triggers[0];
            }

            return null;
        }

        #endregion
    }
}
