﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using TheBackupApp.ClientAgent.Services;

namespace TheBackupApp.ClientAgent
{
    internal static class Utils
    {
        public static string GetLocalIPAddress()
        {
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork))
            {
                return ip.ToString();
            }

            throw new Exception("Local IP Address Not Found!");
        }

        public static async Task ProcessBackupConfig(BackupConfigDTO backupConfig, CommunicationService commService)
        {
            // If not enabled, Update Scheduler to hourly probe and return.
            if (!backupConfig.Enabled)
            {
                SchedulerService.ConfigureScheduler(GetCommand(), 0, 1);
                await commService.LogActivityAsync("Backup is disabled. Updated scheduler to hourly probe.");
                return;
            }

            // Fetch scheduler details
            // Remember, the schedules are in military format (13:15 hrs)
            string schedule = SchedulerService.GetCurrentScheduledTime();
            decimal schedulerTime = decimal.Parse(schedule);
            decimal configTime = decimal.Parse(backupConfig.ScheduledAt);

            // If, Config and scheduler time differ (implying config changed)
            if (configTime != schedulerTime)
            {
                if (configTime < schedulerTime)
                {
                    // If, time <= now, update scheduler and run backup
                    await UpdateScheduler(backupConfig.ScheduledAt, commService);
                    await RunBackup(backupConfig, commService);
                }
                else
                {
                    await UpdateScheduler(backupConfig.ScheduledAt, commService);
                }
            }
            else
            {
                // Else, run normal backup.
                await RunBackup(backupConfig, commService);
            }
        }

        /// <summary>
        /// Returns the current executable path, for use with the task scheduler.
        /// </summary>
        /// <returns></returns>
        public static string GetCommand()
        {
            return System.Reflection.Assembly.GetEntryAssembly().Location;
        }

        private static async Task UpdateScheduler(string scheduledAt, CommunicationService commService)
        {
            SchedulerService.ConfigureScheduler(
                        GetCommand(),
                        Convert.ToDouble(scheduledAt),
                        24);
            await commService.LogActivityAsync($"Agent scheduler updated to {scheduledAt} hrs successfully.");
        }

        private static async Task RunBackup(BackupConfigDTO backupConfig, CommunicationService commService)
        {
            foreach (string sourcePath in backupConfig.SourcePaths)
            {
                BackupService.RunBackup(sourcePath, backupConfig.DestinationPath);
                await commService.LogActivityAsync($"Backup from {sourcePath} to {backupConfig.DestinationPath} completed successfully.");
            }
        }
    }
}
