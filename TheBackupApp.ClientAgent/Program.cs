﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TheBackupApp.ClientAgent.Services;

namespace TheBackupApp.ClientAgent
{
    class Program
    {
        static void Main(string[] args)
        {
            string localIp = Utils.GetLocalIPAddress();
            var commService = new CommunicationService(localIp);

            // With the -Install switch..................
            if (args.Length == 1 && args[0].Equals("Install"))
            {
                if (commService.IsClientRegisteredAsync().Result)
                {
                    FetchAndProcessBackupConfig(commService);
                }
                else
                {
                    // Register client.
                    Task<bool> registerTask = commService.RegisterClientAsync();

                    // Configure scheduler for 60 min probes.
                    SchedulerService.ConfigureScheduler(Utils.GetCommand(), 0, 1);
                    registerTask.Wait();

                    commService.LogActivityAsync("Client registered and agent scheduler configured successfully.").Wait();
                }
            }
            else  // Invocation via scheduler
            {
                commService.LogActivityAsync("Agent scheduler invocation.").Wait();
                FetchAndProcessBackupConfig(commService);
            }
        }

        private static void FetchAndProcessBackupConfig(CommunicationService commService)
        {
            IEnumerable<BackupConfigDTO> backupConfigs = commService.FetchBackupConfigurationAsync().Result;

            foreach (BackupConfigDTO backupConfig in backupConfigs)
            {
                Utils.ProcessBackupConfig(backupConfig, commService).Wait();
            }
        }
    }
}
